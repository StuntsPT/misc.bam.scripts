## Misc.BAM.scripts

Repository for misc. scripts that handle bam files

### maniest_parser.py

This script will read a "Manifest file", parse the coordinates, and then look to the povided BAM files to see which regions are below the defined coverage.

Requires `pysam`

**Usage:**

```
python3 manifest_parser.py "Manifest_file.txt" "min_cov(int)" "flank_region(int)" files.bam 
```

You can either pass each `.bam` file individually or with a shell expression, such as `*.bam`

The results are printed to STDOUT, so you may want to redirect the output to a file.
