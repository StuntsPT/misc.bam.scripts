#!/usr/bin/env python3

# Copyright 2018-2019 Francisco Pina Martins <f.pinamartins@gmail.com>
# manifest_parser.py is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# Loci_counter is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with Loci_counter. If not, see <http://www.gnu.org/licenses/>.

# This script will compare two groups of individuals and highlight any
# loci that segregate both groups

# Usage: python3 manifest_parser.py manifest.file min_cov(int) \
# flank_value(int) bam_file(s)

import pysam
import itertools


def manifest2list(manifest_filename, min_cov, flank_value, bam_files):
    """
    Parses a manifest file and prints each line with an added string
    containing the low coverage zones
    """
    mfile = open(manifest_filename, 'r')
    old_name = ""
    flank_value = int(flank_value)
    header = True
    previous_line = None
    manifest = []
    skip = False

    for lines in mfile:
        lines = lines.strip() + "\t"
        if header is True:
            if lines.startswith("Name\tChromosome"):
                lines = lines + "\t".join(bam_files)
                header = False
                skip = True
            ap_lines = lines
        else:
            if lines.split(".")[0] == lines:
                name = "".join(lines.split("_")[0:2])
            else:
                name = lines.split(".")[0]
            if name == old_name:
                new_coord = str(int(lines.split()[2]) + flank_value)
                lines = lines.replace(lines.split()[2], new_coord)
                try:
                    new_coord2 = str(int(previous_line.split()[3])
                                     - flank_value)
                    previous_line = previous_line.replace(
                        previous_line.split()[3], new_coord2)
                except AttributeError:
                    pass
            else:
                old_name = name
            try:
                ap_lines = line_parser(previous_line, bam_files, min_cov)
            except AttributeError:
                pass
            previous_line = lines
        if skip is False:
            manifest.append(ap_lines)
        else:
            skip = False

    new_coord2 = str(int(lines.split()[3]) + flank_value)
    previous_line = previous_line.replace(lines.split()[3], new_coord2)
    manifest.append(line_parser(previous_line, bam_files, min_cov))

    [print(x) for x in manifest]


def line_parser(line, bam_files, min_cov):
    """
    Parses a single Manifest file line and returns a string with coordinates
    """
    final_ranges = ""
    min_cov = int(min_cov)
    for bam in bam_files:
        coords = line.split()[1:4]
        coords = coords[0] + ":" + coords[1] + "-" + coords[2]
        low_covs = pysam.depth("-aa", "-r", coords, bam, split_lines=True)

        low_coords = list(map(lambda x: int(x.split("\t")[1]) if
                              int(x.split("\t")[2]) < min_cov else None,
                              low_covs))
        low_coords = list(filter(None, low_coords))

        low_cov_ranges = list(range_maker(low_coords))
        sanitized_ranges = [str(x[0]) + "-" + str(x[1]) for x in low_cov_ranges]
        sanitized_ranges = "\t" + ";".join(sanitized_ranges)
        final_ranges += sanitized_ranges

    line += final_ranges

    return line


def range_maker(t_range):
    """
    Take a list of numbers as input and returns a list of ranges
    https://stackoverflow.com/a/43091576/3091595
    """
    t_range = sorted(set(t_range))
    for _, group in itertools.groupby(enumerate(t_range),
                                      lambda t: t[1] - t[0]):
        group = list(group)
        yield group[0][1], group[-1][1]


if __name__ == "__main__":
    from sys import argv
    manifest2list(argv[1], argv[2], argv[3], argv[4:])
